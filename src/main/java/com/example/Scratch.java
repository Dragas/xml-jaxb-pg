package com.example;
import javax.xml.bind.*;
import javax.xml.bind.annotation.*;
import javax.xml.namespace.QName;
import javax.xml.stream.*;
import java.io.ByteArrayOutputStream;

class Scratch {
    public static void main(String[] args) throws JAXBException, XMLStreamException {
        JAXBContext context = JAXBContext.newInstance(StringCont.class);
        StringCont content = new StringCont();
        content.it = "<xml>content</xml>";
        QName q = new QName("", "strwrap");
        JAXBElement<StringCont> jaxbElement = new JAXBElement<>(q, StringCont.class, content);
        Marshaller m = context.createMarshaller();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        XMLOutputFactory outputFactory = XMLOutputFactory.newInstance();
        //outputFactory.setProperty("escapeCharacters", false);
        XMLStreamWriter delegate = outputFactory.createXMLStreamWriter(baos);
        m.marshal(jaxbElement, delegate);
        System.out.printf("%s%n", baos);
    }

    @XmlType(name = "strwrap")
    static class StringCont {
        @XmlElement
        String it;
    }
}
