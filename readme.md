# XML Inlining Playground

Asserts how to inline xml structures with jaxb.

## Running

To run with xerces (jaxb-impl implementation), run only with xerces profile
```shell
mvn compile exec:exec -Dexec.executable=java -Pxerces
```
expected output
```xml
<?xml version="1.0" ?><strwrap><it>&lt;xml&gt;content&lt;/xml&gt;</it></strwrap>
```

To run with woodstox, enable woodstox profile in addition to xerves.
```shell
mvn compile exec:exec -Dexec.executable=java -Pxerces,woodstox
```
expected output
```xml
<?xml version='1.0' encoding='UTF-8'?><strwrap><it>&lt;xml>content&lt;/xml></it></strwrap>
```
